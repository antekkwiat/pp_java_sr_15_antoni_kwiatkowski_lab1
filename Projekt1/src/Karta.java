
public class Karta {
	public enum Figura 
	{
		f2(2),f3(3),f4(4),f5(5),f6(6),f7(7),f8(8),f9(9),f10(10), Walet(11), Dama(12), Krol(13), As(14);
		private Figura(int _wartosc)	
		{
			wartosc=_wartosc;
		}
		public int wartosc;
	}
	enum Kolor
	{
		Trefl, Kier, Pik, Karo;
	}
	
	public Figura f;
	public Kolor k;
	
	@Override//do debugowania
	public String toString() 
	{
		return "Karta [f=" + f + ", k=" + k + "]";
	}

	public Karta(String ciag)
	{
		switch(ciag.charAt(0))//rozpoznaje figur�
		{
		case '2':
			f=Figura.f2;
			break;
		case '3':
			f=Figura.f3;
			break;
		case '4':
			f=Figura.f4;
			break;
		case '5':
			f=Figura.f5;
			break;
		case '6':
			f=Figura.f6;
			break;
		case '7':
			f=Figura.f7;
			break;
		case '8':
			f=Figura.f8;
			break;
		case '9':
			f=Figura.f9;
			break;
		case 'T':
			f=Figura.f10;
			break;
		case 'J':
			f=Figura.Walet;
			break;
		case 'Q':
			f=Figura.Dama;
			break;
		case 'K':
			f=Figura.Krol;
			break;
		case 'A':
			f=Figura.As;
			break;
		}
		
		switch(ciag.charAt(1))//rozpoznaje kolor
		{
		case 'D':
			k=Kolor.Karo;
			break;
		case 'S':
			k=Kolor.Pik;
			break;
		case 'H':
			k=Kolor.Kier;
			break;
		case 'C':
			k=Kolor.Trefl;
			break;
		}
		
	}

}

