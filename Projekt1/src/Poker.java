import java.io.IOException;
import java.util.Scanner;
import java.io.File;

public class Poker 
{
	public enum Uklad 
	{
		WysokaKarta(1),Para(2),DwiePary(3),Trojka(4),Strit(5),Kolor(6),Ful(7),Kareta(8),Poker(9),PokerKrolewski(10);
		public int wartosc;
		private Uklad(int _wartosc)	
		{
			wartosc=_wartosc;
		}
	}
	
	
	Karta[] Gracz1 = new Karta[5];//r�ka 1 gracza
	Karta[] Gracz2 = new Karta[5];//analogicznie
	
	
	public int rozegraj(String nazwa)  throws IOException
	{
		Scanner odczyt = new Scanner(new File(nazwa));
		int Wygrane1=0;//Ilosc wygranych gracza nr 1
		while(odczyt.hasNext())//je�eli by�o kolejne rozdanie
		{
			String Linia[] = odczyt.nextLine().split(" ");//kolejne karty rozdzielone spacj�
			for(int i=0; i<5; i++)
				Gracz1[i]=new Karta(Linia[i]);
			for(int i=5; i<10; i++)
				Gracz2[i-5]=new Karta(Linia[i]);
			if(Porownaj())//Sprawdza ktory gracz wygral. true, gdy gracz 1
				Wygrane1++;
		}
		odczyt.close();
		return Wygrane1;
	}
	
	
	private boolean Porownaj()
	{
		Uklad ukl1=Rozpoznaj(Gracz1), ukl2=Rozpoznaj(Gracz2);//Sprawdza ktory z podstawowych ukladow ma kazdy z graczy
		if(ukl1.wartosc>ukl2.wartosc)
			return true;
		if(ukl1.wartosc<ukl2.wartosc)
			return false;
		
		switch(ukl1)//gdy uklady s� takie same
		{
		case Para:
		case Trojka:
		case Kareta:
			return PorowanajOdIdentycznych();//Porownuje pary, trojki i karety
		case DwiePary:
			return false;//Taka sytuacja w pliku nie wyst�puje wi�c jest nieobs�ugiwana
		case Ful:
			return false;//Taka sytuacja w pliku nie wyst�puje wi�c jest nieobs�ugiwana
		default:
			return PorowanajNajwyzsze();//Pozostale uklady zaleza od najwyzszych kart
		}
	}
	
	
	private Uklad Rozpoznaj(Karta[] karta) 
	{
		SortowanieBabelkowe(karta);
		int i;
		
		for(i=0;i<4 && karta[i].f.wartosc+1==karta[i+1].f.wartosc;i++);//Czy karty nastepuja po sobie
		if(i==4 || (i==3 && karta[0].f==Karta.Figura.f2 && karta[4].f==Karta.Figura.As))//czy spelnia wymagania strita
			if(karta[0].k==karta[1].k && karta[0].k== karta[2].k && karta[0].k == karta[3].k && karta[0].k== karta[4].k)//czy jest jednego koloru
				if(karta[3].f==Karta.Figura.Krol)//czy to poker krolewski
					return Uklad.PokerKrolewski;
				else
					return Uklad.Poker;
			else return Uklad.Strit;
		
		if(karta[0].k==karta[1].k && karta[0].k== karta[2].k && karta[0].k == karta[3].k && karta[0].k== karta[4].k)//czy jest jednego koloru
			return Uklad.Kolor;
		
		i=0;
		for(int j=0;j<4;j++)
			for(int k=j+1;k<5;k++)
				if(karta[j].f==karta[k].f)
					i++;//zlicza ilosc porownan, w ktorych karty okazaly sie identyczne
		
		switch(i)//tak, to zawsze dziala
		{
		case 1:
			return Uklad.Para;
		case 2:
			return Uklad.DwiePary;
		case 3:
			return Uklad.Trojka;
		case 4:
			return Uklad.Ful;
		case 6:
			return Uklad.Kareta;
		}
		return Uklad.WysokaKarta;
	}
	private Karta[] SortowanieBabelkowe(Karta[] k) {
        for (int i = 0; i < k.length; i++) {
            for (int j = 0; j < k.length - 1; j++) {
                if (k[j].f.wartosc > k[j + 1].f.wartosc) {
                    Karta temp;
                    temp = k[j + 1];
                    k[j + 1] = k[j];
                    k[j] = temp;
                }
            }
        }
        return k;
    }
	private boolean PorowanajNajwyzsze()
	{
		for(int i=4; i>=0; i--)//porownuje od najwyzszczej do najnizszej
			if(Gracz1[i].f.wartosc>Gracz2[i].f.wartosc)
				return true;
			else
			if(Gracz1[i].f.wartosc<Gracz2[i].f.wartosc)
				return false;
	return false;
	}
	
	private boolean PorowanajOdIdentycznych()
	{
		int k=1,l=0;
		for(int i=0;i<4;i++)
			for(int j=i+1;j<5;j++)
				if(Gracz1[i].f==Gracz1[j].f)
					k=i;//zapisuje indeks karty z pary, trojki, karety
		for(int i=0;i<4;i++)
			for(int j=i+1;j<5;j++)
				if(Gracz2[i].f==Gracz2[j].f)
					l=i;//jak wyzej
		if(Gracz1[k].f.wartosc>Gracz2[l].f.wartosc)
			return true;
		else
		if(Gracz1[k].f.wartosc<Gracz2[l].f.wartosc)
			return false;
		
	for(int m=4, n=4; m>=0 && n>=0; m--, n--)//porownuje pozostale karty
		{
			while(Gracz1[m].f.wartosc==Gracz1[k].f.wartosc)//pomija karty z ukladu
			{
				m--;
				if(m<0)
					return false;
			}
			while(Gracz2[n].f.wartosc==Gracz2[l].f.wartosc)//analogicznie
			{
				n--;
				if(n<0)
					return false;
			}
			if(Gracz1[m].f.wartosc>Gracz2[n].f.wartosc)
				return true;
			if(Gracz1[m].f.wartosc<Gracz2[n].f.wartosc)
				return false;
		}
	return false;
	}
}
