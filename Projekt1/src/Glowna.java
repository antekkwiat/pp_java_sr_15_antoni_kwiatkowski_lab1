import java.io.IOException;
import java.util.Scanner;
import java.io.File;

public class Glowna {

	public static int fibonacci() {
		int x1, x2, tmp, suma;
		x1 = 1;
		x2 = 2;
		suma = 0;
		if (x1 % 2 == 0)//je�eli parzysty - dodaj
			suma += x1;
		do{
			if (x2 % 2 == 0)
				suma += x2;//je�eli parzysty - dodaj
			tmp = x1 + x2;
			x1 = x2;
			x2 = tmp;//nastepny element

		}while(x2 <= 4000000);
		return suma;
	}

	public static boolean czyPlikIstnieje(String nazwa) {
		File f = new File(nazwa);
		return f.exists() && f.isFile();
	}
	
public static int trojkat(String nazwa)  throws IOException
{
	Scanner sc = new Scanner(new File(nazwa));
	sc.useDelimiter("\\s*\",\"\\s*");//kolejne frazy rozdzielane za pomoca ","
	int ilosc_trojkatnych =0;
	while(sc.hasNext())//czy jest nastepna fraza
	{
		String slowo = sc.next().replaceAll("\"", "");//usuwa cudzyslowy ktore pozostaly
		int liczba=0;//liczba reprezentujaca slowo
		for(int i=0; i<slowo.length(); i++)
		{
			liczba+=(int)slowo.charAt(i)-64;//dodaje numer wielkiej litery z alfabetu
		}
		double n = Math.sqrt((8*liczba)+1);//je�eli liczba jest trojkatna, to n jest calkowite
		if (n == (int)n)
			ilosc_trojkatnych++;
	}
	sc.close();
	return ilosc_trojkatnych;
}

	public static void main(String[] args)  throws IOException{
		// TODO Auto-generated method stub
		System.out.println("hello world!");//Pierwszy program w Javie - trzeba si� przywita�
		System.out.println("��dana suma elementow ci�gu fibonacciego to: " + fibonacci());
		if (czyPlikIstnieje("slowa.txt"))
		{
			int a = trojkat("slowa.txt");
			System.out.println("Jest " + Integer.toString(a) + " liczb tr�jk�tnych");
		}
		Poker poker = new Poker();
		System.out.println("Gracz nr 1. wygra� " + poker.rozegraj("poker.txt") + " razy");
		
			 

	}

}